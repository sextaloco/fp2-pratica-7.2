
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import utfpr.ct.dainf.if62c.pratica.ContadorPalavras;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author leandro
 */
public class Pratica72 {
    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);
        String file = scanner.next();
        ContadorPalavras contador = new ContadorPalavras(file);
        
        Map<String,Integer> maoe = contador.getPalavras();
       
       
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file + ".out"))) {
            String linha;
            for (Map.Entry<String, Integer> entry : maoe.entrySet()) {
                linha = entry.getKey() + "," + entry.getValue();
                bw.write(linha);
                bw.newLine();
            }
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(Pratica72.class.getName()).log(Level.SEVERE, null, ex);
        }
            
            
    }
}
